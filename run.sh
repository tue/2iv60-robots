#!/bin/sh
# Mesa 10 crashes with software rendering. To get faster speed on Intel, patch
# src/robotrace/Base.java to use GLCanvas.
LIBGL_ALWAYS_SOFTWARE=0
MESA_ROOT=/tmp/mesa-root

if [ $LIBGL_ALWAYS_SOFTWARE -ne 0 ] && [ ! -d "$MESA_ROOT" ]; then
    mkdir "$MESA_ROOT"

    # Arch Linux: use older versions (note: LLVM-3.4 still gets loaded somehow
    # which triggers a crash on close).
    for pkg in \
        mesa-9.2.4-1 \
        mesa-libgl-9.2.4-1 \
        llvm-libs-3.3-1 \
        ; do
        tar xf /var/cache/pacman/pkg/$pkg-x86_64.pkg.tar.xz -C "$MESA_ROOT"
    done
fi

if [ $# -eq 0 ]; then
    set -- java -ea -cp lib/gluegen-rt.jar:lib/jogl-all.jar:build/classes RobotRace
elif [ $1 = gdb ]; then
    p="$(dirname "$0")"
    set -- "$@" -q --args java -ea -cp "$p/lib/gluegen-rt.jar:$p/lib/jogl-all.jar:$p/build/classes" RobotRace
    unset p
fi

if [ $LIBGL_ALWAYS_SOFTWARE -eq 0 ]; then
    "$@"
    exit
fi

#LD_DEBUG=all LD_DEBUG_OUTPUT=/tmp/dbg \
LIBGL_ALWAYS_SOFTWARE=1 \
LIBGL_DRIVERS_PATH="$MESA_ROOT/usr/lib/xorg/modules/dri" \
EGL_DRIVERS_PATH="$MESA_ROOT/usr/lib/egl" \
LD_LIBRARY_PATH="$MESA_ROOT/usr/lib" \
"$@"
# vim: set sw=4 et ts=4:
