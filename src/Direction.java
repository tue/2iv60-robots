/**
 * Specifies the direction of a line segment.
 */
public enum Direction {
    X, Y, Z
}
