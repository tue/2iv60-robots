
/**
 * Provides details for animating a walk.
 *
 * @author Peter Wu
 */
interface WalkAnimation {

    /**
     * Sets the new position for the robot.
     *
     * @param pos Position in meters.
     */
    void updatePosition(double pos);

    /**
     * Finds the angle between the left upper leg and the rotated upper leg. If
     * the robot does not move, the angle is probably 0. When the leg is
     * positioned behind the robot, the angle is negative. Similarly, when the
     * robot is positioned forward, the angle is positive.
     *
     * @return angle in degrees.
     */
    public double getLegAngleLeft();

    /**
     * Finds the angle between the right upper leg and the rotated upper leg. If
     * the robot does not move, the angle is probably 0. When the leg is
     * positioned behind the robot, the angle is negative. Similarly, when the
     * robot is positioned forward, the angle is positive.
     *
     * @return angle in degrees.
     */
    public double getLegAngleRight();

    /**
     * Finds the angle behind the left knee.
     *
     * @return angle in degrees.
     */
    public double getKneeAngleLeft();

    /**
     * Finds the angle behind the right knee.
     *
     * @return angle in degrees.
     */
    public double getKneeAngleRight();

    /**
     * Finds the angle between the left arm and the rotated arm. Similar to
     * <code>getLegAngleLeft()</code>.
     *
     * @return angle in degrees.
     */
    public double getArmAngleLeft();

    /**
     * Finds the angle between the right arm and the rotated arm. Similar to
     * <code>getLegAngleRight()</code>.
     *
     * @return angle in degrees.
     */
    public double getArmAngleRight();

    /**
     * Finds the distance between the floor and the bottom of the body.
     *
     * @return A distance in meters.
     */
    public double getBottomOffset();
}
