import java.awt.Color;
import javax.media.opengl.GL2;
import robotrace.Vector;

/**
 * Describes the trees that are placed on the terrain.
 *
 * @author Haterd
 */
public class Tree extends BetterBase {

    /**
     * Defines the scale of the tree, so trees can be varied in size.
     */
    private final float treeScale;
    /**
     * Defines the variation of the tree. There are two variations, and these
     * are set with the values 0 and 1.
     */
    private final int treeVariation;

    /**
     * Defines the position where the tree is placed on the terrain.
     *
     * The position is defined from in both x and y direction as [-20, 20]
     * so position transformations are based on the origin.
     */
    private Vector treePosition;

    /**
     * The display list containing the tree.
     */
    private int treeDisplayList;

    private final float treeTrunkHeight;
    private final float treeTrunkRadius;
    private final Terrain terrain;

    /**
     * Initialize a tree.
     *
     * @param terrain
     * @param position
     * @param scale
     * @param variation
     */
    public Tree(Terrain terrain, Vector position, float scale, int variation) {
        // Set up variables
        this.terrain = terrain;
        this.treeVariation = variation;
        this.treeScale = scale;
        this.treeDisplayList = 0;

        // Set properties of a tree
        this.treeTrunkHeight = 2.5f;
        this.treeTrunkRadius = 0.25f;


        // Set the height based on the terrains height
        this.treePosition = position;
    }

    public void createTree() {
        // Create display list
        this.treeDisplayList = gl.glGenLists(1);

        // Initialize display list
        gl.glNewList(treeDisplayList, GL2.GL_COMPILE);

        // Push matrix so we can restore the state after drawing
        gl.glPushMatrix();

        // Set the position of the tree and the right height
        gl.glTranslatef((float) treePosition.x(), (float) treePosition.y(),
                (float) treePosition.z() - 0.2f);

        // Translate so the tree appears on the right place
        gl.glTranslatef(-treeTrunkRadius / 2, -treeTrunkRadius / 2,
                0);

        // Now draw the trunk
        drawTrunk();

        // Translate to trunk height and draw the leaves
        drawLeaves();

        // Restore drawing state
        gl.glPopMatrix();

        /** End the list, everything in the list can be easily drawed by using
        *   this display list. This is more efficient then just drawing
        *   everything every time.
        */
        gl.glEndList();
    }

    /**
     * Draw this tree, this method will be called from the Terrain class
     * that will contain all trees in an array.
     */
    public void drawTree() {
        gl.glCallList(treeDisplayList);
    }

    // Draw the trunk of the tree.
    private void drawTrunk() {
        setColor(new Color(83, 53, 10));
        glut.glutSolidCylinder(treeTrunkRadius, treeTrunkHeight + treeScale, 6, 6);
    }

    /**
     * Draw the leaves of the tree, based on the variation.
     */
    private void drawLeaves() {

        float totalHeight = treeScale + treeTrunkHeight;

        if(treeVariation == 0) {
            // Draw the christmas tree
            setColor(new Color(20, 51, 6));
            gl.glTranslatef(0, 0, totalHeight - 0.5f);
            glut.glutSolidCone(4f * treeTrunkRadius, totalHeight / 1.2f, 6, 6);
            gl.glTranslatef(0, 0, 1);
            setColor(new Color(27, 70, 9));
            glut.glutSolidCone(3.25f * treeTrunkRadius, totalHeight / 1.5f, 6, 6);
        } else {
            // Draw the normal spherish tree
            float radius = treeTrunkRadius * 4.5f;
            setColor(new Color(20, 51, 6));
            gl.glTranslatef(0, 0, totalHeight - (radius / 2));
            glut.glutSolidSphere(radius, 6, 6);
            setColor(new Color(27, 70, 9));
            gl.glTranslatef(0, 0, radius);
            glut.glutSolidSphere(radius / 1.25f, 6, 6);
        }
    }

    public Vector getPosition() {
        return treePosition;
    }




}
