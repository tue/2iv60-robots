
import java.awt.Color;

/**
 * A collection of colors.
 */
public class Colors {

    /**
     * Made up color names.
     */
    public static Color BLUEISH = new Color(100, 130, 255);
    public static Color GRAYISH = new Color(100, 100, 100);
    public static Color DIRTY_BLUE = new Color(130, 158, 174);
    public static Color ARM_GRAY_COLOR = new Color(200, 200, 200);

    /**
     * Names taken from Wikipedia.
     *
     * @see https://en.wikipedia.org/wiki/List_of_colors:_A%E2%80%93F
     */
    public static Color LAVENDER = new Color(0xE6E6FA);
    public static Color PALE_TURQOISE = new Color(0xAFEEEE);
    public static Color CHOCOLATE = new Color(0x7B3F00);
}
