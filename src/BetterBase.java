
import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import java.awt.Color;
import robotrace.Vector;
import static javax.media.opengl.GL2.*;

/**
 * Base class that provides basic bindings to the ugly JOGL interface. This
 * works around the limitations imposed by JOGL.
 */
abstract class BetterBase {

    /**
     * OpenGL context.
     */
    protected static GL2 gl;

    /**
     * OpenGL Utility instance.
     */
    protected static GLU glu;
    /**
     * OpenGL Utility Toolkit instance.
     */
    protected static GLUT glut;

    public static void setGL(GL2 gl) {
        BetterBase.gl = gl;
    }

    public static void setGLU(GLU glu) {
        BetterBase.glu = glu;
    }

    public static void setGLUT(GLUT glut) {
        BetterBase.glut = glut;
    }

    /**
     * Utility method to set color.
     *
     * @param color An AWT color.
     */
    static void setColor(Color color) {
        // contains four RGBA color components (floats in range 0 to 1)
        float[] rgba = color.getRGBComponents(null);
        gl.glColor3fv(rgba, 0);
    }

    /**
     * Pass a vector as a vertex to OpenGL.
     */
    static public void glVertex(Vector vector) {
        gl.glVertex3d(vector.x(),
                      vector.y(),
                      vector.z());
    }

    /**
     * Use given vector as normal.
     */
    static public void glNormal(Vector vector) {
        gl.glNormal3d(vector.x(),
                      vector.y(),
                      vector.z());
    }

    /**
     * Unbind the textures so the scene is drawn correctly.
     */
    static public void unbindTextures() {
        gl.glBindTexture(GL_TEXTURE_2D, 0);
    }
}
