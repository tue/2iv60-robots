/**
 * Materials that can be used for the robots.
 */
public enum Material {

    /**
     * Gold material properties.
     * Modify the default values to make it look like gold.
     */
    GOLD (
        new float[] {1f, 0.5f, 0f, 1.0f},
        new float[] {1f, 0.5f, 0f, 1.0f}),

    /**
     * Silver material properties.
     * Modify the default values to make it look like silver.
     */
    SILVER (
        new float[] {0.35f, 0.35f, 0.35f, 1.0f},
        new float[] {0.508273f, 0.508273f, 0.508273f, 1.0f}),

    /**
     * Wood material properties.
     * Modify the default values to make it look like wood.
     */
    WOOD (
        new float[] {0.59f, 0.36f, 0.0588f, 1.0f},
        new float[] {0.1f, 0.1f, 0.1f, 1.0f}),

    /**
     * Orange material properties.
     * Modify the default values to make it look like orange.
     */
    ORANGE (
        new float[] {0.5f, 0.2f, 0f, 1.0f},
        new float[] {0.5f, 0.2f, 0f, 1.0f});

    /** The diffuse RGBA reflectance of the material. */
    float[] diffuse;

    /** The specular RGBA reflectance of the material. */
    float[] specular;

    /**
     * Constructs a new material with diffuse and specular properties.
     */
    private Material(float[] diffuse, float[] specular) {
        this.diffuse = diffuse;
        this.specular = specular;
    }
}
